# Web site for cactus fans
Site describes different types of cactus, provides a little info about it. Good for gardeners.

# Motivation
Web site created as the final assignment for Web Foundation course. The main goal was to get the highest mark.

# Tech/framework used
Built with:
- Brackets

# Installation
To see the content of the site open index.html file and browse the content. To go through the code open files in any code editor or just use text editor for it.

# License 
This project is licensed under the MIT License - see the LICENSE.md file for details. I chose this license type because it's the best type of license for free software and I provide it "as is" without any warranty.
 